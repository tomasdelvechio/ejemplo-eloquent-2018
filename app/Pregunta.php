<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    protected $guarded = [];

    const DIFICULTAD = [
        'Baja',
        'Media',
        'Alta',
    ];

    /**
     * Accessor para campo Dificultad
     */
    public function getDificultadAttribute($dificultad)
    {
        return self::DIFICULTAD[$dificultad];
    }

    /**
     *  Mutator del tipo Dificultad
     */
    public function setDificultadAttribute($dificultad)
    {
        $this->attributes['dificultad'] = array_search($dificultad, self::DIFICULTAD);
    }

    public function respuestas()
    {
        return $this->hasMany('App\Respuesta');
    }
}
