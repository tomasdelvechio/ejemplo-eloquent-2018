<?php

use Illuminate\Database\Seeder;
use App\Pregunta;

class PreguntasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pregunta::truncate();
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            Pregunta::create([
                'pregunta' => $faker->text(30),
                'dificultad' => $faker->randomElement(Pregunta::DIFICULTAD),
            ]);
        }

    }
}
