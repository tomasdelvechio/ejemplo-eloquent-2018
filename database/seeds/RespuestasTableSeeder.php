<?php

use Illuminate\Database\Seeder;
use App\Respuesta;
use App\Pregunta;

class RespuestasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Respuesta::truncate();
        $faker = \Faker\Factory::create();

        $preguntas_ids = Pregunta::select('id')->get();

        foreach ($preguntas_ids as $pregunta_id) {
            $correcta = true;
            for ($i = 0; $i < 3; $i++) {
                Respuesta::create([
                    'respuesta' => $faker->text(30),
                    'pregunta_id' => $pregunta_id->id,
                    'correcta' => $correcta,
                ]);
                $correcta = false;
            }
        }
    }
}
